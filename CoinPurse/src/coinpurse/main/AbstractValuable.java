package coinpurse.main;
/**
 * 
 * Super Class for Coin Banknote Coupon.
 * @author Arut Thanomwatana
 *
 */

public abstract class AbstractValuable implements Valuable {

	
	
	/**
	 * Check if this and other Coupon has the same value.
	 * @param obj is another Coupon.
	 * @return true if this and other Coupon have the same value
	 */
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj.getClass() != this.getClass())
			return false;
		
		Valuable other = (Valuable)obj;
		
		if(this.getValue() == other.getValue())
			return true;
		
		return false;
	}
	
	/**
     * Compare coins by value.
     * @param valuable 
     * @return -1 if this valuable is null or has less value than other valuable
     * 		    0 if this valuable has same value with other valuable
     *          1 if this valuable has more value than other valuable
     */
	
	public int compareTo(Valuable valuable){
		if(valuable == null)
			return -1;
		
		if(this.getValue() < valuable.getValue())
			return -1;
		else if(this.getValue() > valuable.getValue())
			return 1;
		else
			return 0;
	}
	
	 /**Get the value of Valuable.
     * @return value of the Valuable 
     */
	
	public abstract double getValue();
	
	

}