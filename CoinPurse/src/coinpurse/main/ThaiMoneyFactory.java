package coinpurse.main;
import java.util.Arrays;
import java.util.List;

/**
 * Money Factory for Thai currency
 * @author Arut Thanomwatana
 *
 */
public class ThaiMoneyFactory extends MoneyFactory
{
	/**List of Coin*/
	private final List<Double> coinList = Arrays.asList(1.0,2.0,5.0,10.0);

	/**List of BankNote*/
	private final List<Double> noteList = Arrays.asList(20.0,50.0,100.0,1000.0);
	
	/**
	 * Initialize ThaiMoneyFactory
	 */
	public ThaiMoneyFactory(){
		super();
	}

	/**
	 * create new money.
	 * @param value is the value of new money
	 * @return new money
	 */
	@Override
	public Valuable createMoney(double value){
		
		if(coinList.contains(value))
			return new Coin(value,"Baht");
		else if(noteList.contains(value))
			return new BankNote(value,"Baht");
		else
			throw new IllegalArgumentException();
		
	}

}
