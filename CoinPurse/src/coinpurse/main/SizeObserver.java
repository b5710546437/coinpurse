package coinpurse.main;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class SizeObserver extends JFrame implements Observer
{
	private JLabel size;
	private JProgressBar progress;
	private JPanel pane;
	public SizeObserver()
	{
		super("Purse Size");
	}

	public void initComponent()
	{
		super.setSize(50,100);
		pane = new JPanel();
		pane.setLayout(new BoxLayout(pane,BoxLayout.Y_AXIS));
		size = new JLabel("Empty");
		progress = new JProgressBar();
		pane.add(size);
		pane.add(progress);
		super.add(pane);

	}

	public void update(Observable subject,Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse) subject;
			
			progress.setMaximum(purse.getCapacity());
			progress.setValue(purse.count());
			if(purse.isFull())
				size.setText("Full");
			else if(purse.count()==0)
				size.setText("Empty");
			else
				size.setText(purse.count()+"");
			
			
			
		}
		if(info != null) System.out.println(info);
	}

	public void run(){
		this.initComponent();
		this.setVisible(true);
	}

}
