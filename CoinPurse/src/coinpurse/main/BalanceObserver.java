package coinpurse.main;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class BalanceObserver extends JFrame implements Observer
{
	protected static JLabel balance;
	public BalanceObserver(){
		super("Purse Balance");
	}
	public void initComponent()
	{
		super.setSize(50, 100);
		balance = new JLabel("0.00 Baht.");
		super.add(balance);

	}
	public void run(){
		this.initComponent();
		this.setVisible(true);
	}

	public void update(Observable subject,Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse) subject;
			double balance = purse.getBalance();
			BalanceObserver.balance.setText(balance+" Baht.");
		}
		if(info != null) System.out.println(info);
	}


}
