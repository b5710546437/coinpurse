package coinpurse.main;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Arut Thanomwatana
 * @version 20.01.2015
 */
public class Coin extends AbstractValuable {

    /**Value of the coin. 
     *  
     */
    double value;
    /**coin currency*/
    String currency;
   
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     * @param currency is the currency of the money
     */
    public Coin( double value,String currency ) {
        this.value = value;
        this.currency = currency;
    }

    /**Get the value of coin.
     * @return value of the coin 
     */
    public double getValue()
    {
    	return this.value;
    }
    
    /**Define this object.
     * @return coin value in Bath
     */
    
    public String toString()
    {
    	return String.format("%.0f %s coin", this.value,this.currency);
    }
    
   

    
}
