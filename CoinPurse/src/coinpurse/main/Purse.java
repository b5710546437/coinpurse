package coinpurse.main;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;



/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Arut Thanomwatana
 *  @version 20.01.2015
 */
public class Purse extends Observable{
	/** Collection of coins in the purse. */
	private List <Valuable> money;
	private	final MyComparator valueComparator = new MyComparator();
	WithdrawStrategy strategy;
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {

		money = new <Valuable> ArrayList();
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() {

		return money.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {

		double sum = 0;
		for(int i=0;i<money.size();i++)
		{
			sum+=money.get(i).getValue();
		}
		return sum; 
	}


	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */

	public int getCapacity() {
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {

		if(money.size()==this.capacity)
			return true;
		return false;
	}

	/** 
	 * Insert a coin into the purse.
	 * The coin is only inserted if the purse has space for it
	 * and the coin has positive value.  No worthless coins!
	 * @param value is a Valuable object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {

		if(isFull())
		{
			return false;
		}
		if(value.getValue()<=0)
		{
			return false;
		}
		money.add(value);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		Collections.sort(money,valueComparator);
		Collections.reverse(money);
		setWithdrawStrategy(new GreedyWithdraw());
		List<Valuable> tempList =  strategy.withdraw(amount, money);
		
		if(tempList==null)
		{
			setWithdrawStrategy(new RecursiveWithdraw());
			tempList = strategy.withdraw(amount, money);
		}
		if(tempList==null)
			return null;
		Valuable[] withdraw = new Valuable[tempList.size()];
		for(int i = 0;i<tempList.size();i++)
		{
			withdraw[i] = tempList.get(i);
		}
		
		for(int i =0;i<tempList.size();i++)
		{
			for(int j=0;j<money.size();j++)
			{
				if(tempList.get(i).equals(money.get(j)))
				{
					money.remove(j);
					break;
				}
			}
		}
		
		super.setChanged();
		super.notifyObservers(this);
		return withdraw;
		
		
	}
	/**
	 * setWithdrawStrategy set a strategy depend on user.
	 * @param strategy is a strategy that we want to set
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy)
	{
		this.strategy=strategy;
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return String that define purse.
	 */
	public String toString() {
		
		return String.format("%d coins with value %.1f", count(),getBalance());
	}

}

