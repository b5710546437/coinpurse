package coinpurse.main;

/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Arut Thanomwatana
 * @version 27.01.2015
 */

public class BankNote extends AbstractValuable 
{
	/**serialNumber of the BankNote.*/
	private static int serialNumber = 999999;
	 /** Value of the BankNote. */
	private double value;
	/** Banknote currency*/
	private String currency;
	
	/**
	 * Constructor of BankNote.
	 * @param value is a value for BankNote
	 */
	public BankNote(double value,String currency)
	{
		this.value = value;
		this.currency = currency;
		getNextSerialNumber();
	}
	/**
	 * Run the serial number.
	 * @return serialNumber
	 */
	public int getNextSerialNumber()
	{
		return ++serialNumber;
	}
	
	/**
	 * Get the value of BankNote.
	 * @return value of BankNote
	 */
	public double getValue()
	{
		return this.value;
	}
	/**
	 * Define BankNote.
	 * @return String of BankNote
	 */
	public String toString()
	{
		return this.getValue() + " " + this.currency + " Banknote ["+this.serialNumber+"]";
	}
	
	
	
	

}
