package coinpurse.main;

import java.util.Arrays;
import java.util.List;
/**
 * Money Factory for Malaysia Currency.
 * @author Arut Thanomwatana
 *
 */
public class MalaiMoneyFactory extends MoneyFactory
{
	/**List of Coin */
	private final List<Double> coinList = Arrays.asList(0.05,0.10,0.20,0.50);
	
	/**List of BankNote */
	private final List<Double> noteList = Arrays.asList(1.0,2.0,5.0,10.0,20.0,50.0,100.0);
	
	/**
	 * Initialize MalaiMoneyFactory
	 */
	public MalaiMoneyFactory(){
		super();
	}


	/**
	 * Create new money.
	 * @param value of new money
	 * @return money
	 */
	@Override
	public Valuable createMoney(double value) {
		if(coinList.contains(value))
			return new Coin(value,"Sen");
		else if(noteList.contains(value))
			return new BankNote(value,"Ringgit");
		else
			throw new IllegalArgumentException();
	}
	
	
	

}
