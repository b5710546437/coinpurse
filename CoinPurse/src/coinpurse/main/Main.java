package coinpurse.main;

import java.util.Observer;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Arut Thanomwatana
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	
    	Purse purse = new Purse(10);
    	
    	Observer balanceObserver = new BalanceObserver();
    	purse.addObserver(balanceObserver);
    	((BalanceObserver) balanceObserver).run();
    	Observer sizeObserver = new SizeObserver();
    	purse.addObserver(sizeObserver);
    	((SizeObserver) sizeObserver).run();
    	
    	ConsoleDialog ui = new ConsoleDialog( purse );
    	ui.run();

    }
}
