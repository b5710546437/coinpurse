package coinpurse.main;

import java.util.ResourceBundle;
/**
 * MoneyFactory is the abstact class for every factory.
 * @author Arut Thanomwatana
 *
 */
public abstract class MoneyFactory {

	/**instance of moneyfactory. */
	private static MoneyFactory instance;

	/**
	 * Initialize the MoneyFactory.
	 */
	public MoneyFactory() {
		instance = this;
	}

	/** Get the money factory instance. */
	public static MoneyFactory getInstance() {
		setFactory();
		return instance; 
		}
	
	/** Create new money 
	 * @param value is the value that want to be create
	 * 
	 */ 
	public abstract Valuable createMoney(double value);
	
	/**
	 * Invoke createMoney if value is String
	 * @param value is the value that will be set
	 * @return invoked createMoney
	 */
	public Valuable createMoney(String value){
		
		return createMoney(Double.parseDouble(value));
	}
	
	/**
	 * Set the factory depend on purse.properties
	 */
	public static void setFactory()
	{
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String type = bundle.getString("factoryType");
		try {
			instance = (MoneyFactory) Class.forName(type).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			instance = getDefualtInstance();
			e.printStackTrace();
		}
	}
	
	/**
	 * get Defualt instance if cannot get instance from purse.properties
	 * @return DefualtMoneyFactory
	 */
	private static MoneyFactory getDefualtInstance(){
		return new ThaiMoneyFactory();
	}
	
	
}
