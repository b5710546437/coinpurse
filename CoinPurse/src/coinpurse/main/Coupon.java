package coinpurse.main;
import java.util.HashMap;
import java.util.Map;
/**
 * A Coupon with a monetary value.
 * You can't change the value of a BankNote.
 * @author Arut Thanomwatana
 * @version 27.01.2015
 */

public class Coupon extends AbstractValuable {
	/**Color of Coupon.*/
	private String color;
	/**Map that keep data.*/
	static Map<String,Double> map;
	static{
		map = new HashMap<String,Double>();
		map.put("Red".toLowerCase(), 100.0);
		map.put("Blue".toLowerCase(), 50.0);
		map.put("Green".toLowerCase(), 20.0);
	}
	
	/**
	 * Constructor of Coupon.
	 * @param color is color of Coupon
	 */
	public Coupon(String color)
	{
		this.color = color.toLowerCase();
		
	}
	/**
	 * Get the value of Coupon.
	 * @return value of Coupon
	 */
	public double getValue()
	{
		return map.get(color);
	}
	/**
	 * Define Coupon.
	 * @return String that Define Coupon
	 */
	public String toString()
	{
		return this.color + "Coupon";
	}
	
	

	
}
