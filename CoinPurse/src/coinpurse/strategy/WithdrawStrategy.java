package coinpurse.strategy;

import java.util.List;

import coinpurse.main.Valuable;

/**
 * An strategy for withdraw interface. 
 * @author Arut Thanomwatana
 *
 */
public interface WithdrawStrategy {
	/**
	 * 
	 * @param amount is amount that will be withdraw from purse
	 * @param valuable is a list of valuable that purse contain
	 * @return array of valuable that is withdrawn from purse
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> valuable);

}
