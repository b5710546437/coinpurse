package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.main.Valuable;
/**
 * A Recursive Strategy for withdraw the valuable from purse.
 * @author Arut Thanomwatana
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy
{   
	/**
	 * Withdraw the request money by recursive strategy.
	 * @param amount is amount of valuable that user want to withdraw
	 * @param valuable is a set of valuable that purse contain
	 * @return set of valuable that withdraw from purse
	 */
	public List<Valuable> withdraw(double amount,List <Valuable> valuable)
	{

		List<Valuable> returnTemp = reWithdraw(amount,valuable,0);
		if(returnTemp==null){
			if(valuable.size()==1){
				return null;
			}
			return withdraw(amount,valuable.subList(1, valuable.size()));

			
		}
		return returnTemp;
	}

	/**
	 * reWithdraw use for repick every element in the list by recursion.
	 * @param amount is amount of valuable that user want to withdraw
	 * @param valuable is a set of valuable that purse contain
	 * @param index is the first index of the List
	 * @return set of valuable that withdraw from purse
	 */

	public List<Valuable> reWithdraw(double amount,List<Valuable> valuable,int index)
	{

		if(valuable.size()==0)
			return null;
		if(index==valuable.size())
			return null;

		if(amount<0)
			return null;
		amount-=valuable.get(index).getValue();
		if(amount==0){
			List <Valuable> tempList = new ArrayList();
			tempList.add(valuable.get(index));
			return tempList;
		}

		List <Valuable> returnList = reWithdraw(amount,valuable,index+1);

		if(returnList==null){
			amount+=valuable.get(index).getValue();
			return reWithdraw(amount,valuable,index+1);
		}
		else{
			returnList.add(valuable.get(index));
			return returnList;
		}
	}

	

}
