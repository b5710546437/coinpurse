package coinpurse.strategy;
import coinpurse.main.Valuable;
import coinpurse.main.MyComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * A Greedy Strategy for withdraw the valuable from purse.
 * @author Arut Thanomwatana
 *
 */
public class GreedyWithdraw implements WithdrawStrategy
{
	/**
	 * Withdraw the request money by greedy strategy.
	 * @param amount is amount of valuable that user want to withdraw
	 * @param valuables is a set of valuable that purse contain
	 * @return set of valuable that withdraw from purse
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> valuables)
	{
		
		List <Valuable> tempList = new <Valuable> ArrayList();
		if(amount == 0) return null;
		
		for(int i =0;i<valuables.size();i++)
		{
			if(amount==0)
			{
				break;
			}
			if ( amount < 0 )
			{	
				return null;
			}
			if(valuables.get(i).getValue()>amount)
				continue;

			tempList.add(valuables.get(i));	
			amount-=valuables.get(i).getValue();
			
		}
		
		if(amount!=0)
			return null;

		
		
		
		

		return tempList;
	}

}
