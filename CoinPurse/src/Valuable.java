/**
 * An Interface for any type of money.
 * @author Arut Thanomwatana
 * @version 15.01.27
 *
 */
public interface Valuable {
	/**
	 * Get the value of valuable.
	 * @return value
	 */
	public double getValue();
	/**
	 * Define Valuable.
	 * @return String of Valuable
	 */
	public String toString();
	/**
	 * Check if Valuable has the same value.
	 * @param obj is another Valuable
	 * @return true if they have the same value
	 */
	public boolean equals(Object obj);

}
