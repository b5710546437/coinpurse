
/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Arut Thanomwatana
 * @version 27.01.2015
 */

public class BankNote implements Valuable
{
	/**serialNumber of the BankNote.*/
	private static int serialNumber = 999999;
	 /** Value of the BankNote. */
	private double value;
	
	/**
	 * Constructor of BankNote.
	 * @param value is a value for BankNote
	 */
	public BankNote(double value)
	{
		this.value = value;
		getNextSerialNumber();
	}
	/**
	 * Run the serial number.
	 * @return serialNumber
	 */
	public int getNextSerialNumber()
	{
		return ++serialNumber;
	}
	
	/**
	 * Get the value of BankNote.
	 * @return value of BankNote
	 */
	public double getValue()
	{
		return this.value;
	}
	/**
	 * Check if this and other BankNote is the same.
	 * @param obj is another BankNote
	 * @return true if this and other BankNote has the same value
	 */
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
    	if(obj.getClass()!= this.getClass()) return false;
    	BankNote other = (BankNote) obj;
    	return (this.getValue()==other.getValue());
    	
	}
	/**
	 * Define BankNote.
	 * @return String of BankNote
	 */
	public String toString()
	{
		return this.getValue() + "-Baht Banknote ["+this.serialNumber+"]";
	}
	
	

}
