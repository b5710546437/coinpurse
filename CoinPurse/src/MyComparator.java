import java.util.Comparator;
/**
 * This class is used to compared any Valuable.
 * 
 * @author Arut Thanomwatana
 * @version 15.01.27
 *
 */

public class MyComparator implements Comparator<Valuable>
{
	/**
	 * Compare 2 String a and b.
	 * @return < 0 if a has less value than b,
	 * 		   > 0 if a has more value than b,
	 * = 0 if a and b have same value.
	 */
	public int compare(Valuable a, Valuable b){
		return (int) Math.signum(a.getValue()-b.getValue());
	}

}
