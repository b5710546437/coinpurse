 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Arut Thanomwatana
 * @version 20.01.2015
 */
public class Coin implements Valuable{

    /**Value of the coin. 
     *  
     */
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }

    /**Get the value of coin.
     * @return value of the coin 
     */
    public double getValue()
    {
    	return this.value;
    }
    /**Check this coin value with other coin.
     * @param arg is an object to check value with this coin
     * @return false if arg is null,not Coin class and not the same value with this coin
     * 		   true if 2 coin have the same value
     * 
     */
    public boolean equals(Object arg)
    {
    	if(arg == null) return false;
    	if(arg.getClass()!= this.getClass()) return false;
    	Coin other = (Coin) arg;
    	return (this.value==other.getValue());
    	
    }

    /**
     * Compare coins by value.
     * @param coin 
     * @return -1 if this coin is null or has less value than other coin
     * 		    0 if this coin has same value with other coin
     *          1 if this coin has more value than other coin
     */
   
    
    /**Define this object.
     * @return coin value in Bath
     */
    
    public String toString()
    {
    	return String.format("%.0f Baht", this.value);
    }

    
}
