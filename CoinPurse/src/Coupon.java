import java.util.HashMap;
import java.util.Map;
/**
 * A Coupon with a monetary value.
 * You can't change the value of a BankNote.
 * @author Arut Thanomwatana
 * @version 27.01.2015
 */

public class Coupon implements Valuable{
	/**Color of Coupon.*/
	private String color;
	/**Map that keep data.*/
	Map<String,Double> map;
	
	/**
	 * Constructor of Coupon.
	 * @param color is color of Coupon
	 */
	public Coupon(String color)
	{
		this.color = color.toLowerCase();
		map = new HashMap();
		map.put("Red".toLowerCase(), 100.0);
		map.put("Blue".toLowerCase(), 50.0);
		map.put("Green".toLowerCase(), 20.0);
	}
	/**
	 * Get the value of Coupon.
	 * @return value of Coupon
	 */
	public double getValue()
	{
		return map.get(color);
	}
	/**
	 * Check if this and other Coupon has the same value.
	 * @param obj is another Coupon.
	 * @return true if this and other Coupon have the same value
	 */
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
    	if(obj.getClass()!= this.getClass()) return false;
    	Coupon other = (Coupon) obj;
    	return (this.getValue()==other.getValue());
	}
	/**
	 * Define Coupon.
	 * @return String that Define Coupon
	 */
	public String toString()
	{
		return this.color + "Coupon";
	}

	
}
